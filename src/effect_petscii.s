;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.include "generic_macros.h"
.include "imgdata.h"
.include "doublebuffer.h"
.include "debug.h"

.export _initpetscii
.export _playpetscii


        ;; draw this number of lines on the screen
.DEFINE PAINTLINES 25


.segment "DATA"

framecount:
        .byt 0
img_scroll:
        .byt (scrollwidth - 10)
finescroll:
        .byt 0

petscii_active:
        .byt 0

.segment "CODE"

.proc _initpetscii: near
        lda #1
        sta petscii_active

        rts
.endproc



.proc _playpetscii: near
        lda petscii_active
        beq @skip

        dbg_set_col DCOL_START_PETSCII

        ;; framecount 0-7 finescroll only, then move bytes
        lda framecount
        cmp #08
        bne @ready
        lda #00
        sta framecount

        inc finescroll
        lda finescroll
        cmp #$08
        bne @finescroll
        lda #00
        sta finescroll

        ;; reset y-scroll
        lda #00
        sta $ff07

        jsr _doublebuffer
        jsr copy_imgdata_to_buffer
        jsr shift_bufferdata

        jmp @ready
@finescroll:
        inc $ff07
@ready:
        inc framecount
@skip:

        dbg_set_col DCOL_STOP_PETSCII

        rts
.endproc




shift_bufferdata:

        lda read_chrmem
        sta shr0+2
        lda #00
        sta shr0+1
        lda read_colmem
        sta shr1+2
        lda #00
        sta shr1+1

        lda write_chrmem
        sta shw0+2
        lda #01
        sta shw0+1
        lda write_colmem
        sta shw1+2
        lda #01
        sta shw1+1

        ldx #PAINTLINES
shloop0:
        ldy #00
shloop1:
shr0:   lda $ffff,y
shw0:   sta $ffff,y
shr1:   lda $ffff,y
shw1:   sta $ffff,y
        iny
        cpy #39
        bne shloop1

        add16 shr0+1,#40
        add16 shw0+1,#40
        add16 shr1+1,#40
        add16 shw1+1,#40

        dex
        bne shloop0

        rts



.proc copy_imgdata_to_buffer: near

        lda img_scroll
        cmp #imgwidth
        bcc @docopy
        jmp pcpcont0

@docopy:
        lda write_chrmem
        sta charw+2
        lda write_colmem
        sta colw+2
        lda #00
        sta charw+1
        sta colw+1

        lda #<imgdat0
        sta charr+1
        lda #>imgdat0
        sta charr+2

        lda #<coldat0
        sta colr+1
        lda #>coldat0
        sta colr+2

        add16 charr+1,img_scroll
        add16 colr+1,img_scroll

        ldx #00
pcploop0:
charr:  lda $ffff
charw:  sta $0c00
colr:   lda $ffff
colw:   sta $0800

        add16 charr+1,#imgwidth
        add16 colr+1,#imgwidth
        add16 charw+1,#40
        add16 colw+1,#40
        inx
        cpx #PAINTLINES
        bne pcploop0

        ;; wrap if necessary
        lda img_scroll
        cmp #00
        bne pcpcont0
        lda #scrollwidth
        sta img_scroll
pcpcont0:
        dec img_scroll

@pcpexit:

        rts
.endproc
