;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.ifndef BLACK

.define BLACK   0
.define GREY    1
.define RED     2
.define CYANa   3
.define PINKa   4
.define GREENa  5
.define BLUEa   6
.define YELLOWa 7
.define ORANGEa 8
.define BROWN   9
.define GREENb  10
.define PINKb   11
.define CYANb   12
.define BLUEb   13
.define BLUEc   14
.define GREENc  15

;; brightness level: 0-7  (no effect on BLACK)
.macro lda_col col, level
     lda #(col + (16 * level))
.endmacro

.endif
