;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.include "rasterconf.h"
.include "demo_sequencer.h"

.export _irqinit
.export rasterlineindex


.segment "DATA"

rasterlines:
        .byt RASTERIRQLINES
rasterlineindex:
        .byt 0



.segment "CODE"

.proc _irqinit: near

        ; now set the new interrupt pointer
        sei
        lda #<_interrupt ; point IRQ Vector to our custom irq routine
        ldx #>_interrupt
        sta $314 ; store in $314/$315
        stx $315

        lda #$02
        sta $ff0a               ; enable raster irq and clear high bit of cmp value
        lda #$00                ; top raster line
        sta $ff0b               ; low bit of cmp value

        jsr sequencer_init

        cli ; clear interrupt disable flag
        rts
.endproc


.proc _interrupt: near
        jsr sequencer_play

        ;; maintain rasterline index and irq
        inc rasterlineindex
        lda rasterlineindex
        cmp #nr_irq
        bcc @nowrap
        lda #0
        sta rasterlineindex
@nowrap:
        ldx rasterlineindex
        lda rasterlines,x
        sta $ff0b               ; store in lo raster value

        lda $ff09
        sta $ff09               ; clear all bits that were set

        pla
        tay
        pla
        tax
        pla
        rti
.endproc
