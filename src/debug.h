;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.ifndef DEBUG_LOADED
.define DEBUG_LOADED

.include "cols.h"


        ;; .define DBUG nop                ; on
.define DBUG ; off


.ifnblank DBUG
        .define DCOL_OFF   lda_col GREY,7
        .define DCOL_PART0 lda_col RED,4
        .define DCOL_PART1 lda_col GREENa,4
        .define DCOL_PART2 lda_col PINKa,4
        .define DCOL_PART3 lda_col YELLOWa,4

        .define DCOL_START_PETSCII BLACK
        .define DCOL_STOP_PETSCII  YELLOWa

.macro dbg_part p
        .local chk1
        .local chk2
        .local chk3
        .local chk4
        .local store
        lda p
        cmp #0
        bne chk1
        DCOL_PART0
        jmp store
chk1:   cmp #1
        bne chk2
        DCOL_PART1
        jmp store
chk2:   cmp #2
        bne chk3
        DCOL_PART2
        jmp store
chk3:   cmp #3
        bne chk4
        DCOL_PART3
        jmp store
chk4:   DCOL_OFF
store:  sta $ff19
.endmacro


.macro dbg_set_col c
        lda c
        sta $ff19
.endmacro

.else
        .define DCOL_OFF
        .define DCOL_PART0
        .define DCOL_PART1
        .define DCOL_PART2

        .macro dbg_part p
        .endmacro

        .macro dbg_set_col c
        .endmacro
.endif


.endif
