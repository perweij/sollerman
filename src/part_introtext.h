;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.ifndef introtext_init
.import introtext_init
.import introtext_play
.import introtext_destruct
.import introtext_cleanup
.endif
