;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.ifndef nr_irq

.DEFINE nr_irq  2
.DEFINE RASTERIRQLINES $80, $ff

.DEFINE MUSIC_LINE 0

.DEFINE LINE_OCEAN 0

.endif
