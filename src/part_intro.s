;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.include "rasterconf.h"
.include "effect_petscii.h"
.include "effect_rasterbar.h"
.include "irq.h"
.include "music.h"

.export intro_play
.export intro_init
.export intro_destruct



.proc intro_init: near
        lda #$01     ; select second tune
        jsr sid_init ; init music
        lda #$d0                ; upper case - for the petscii
        sta $ff13
        jsr _initpetscii
        rts
.endproc


.proc intro_play: near
        jsr rasterbar_play
        music_play rasterlineindex,#MUSIC_LINE
        rts
.endproc


.proc intro_destruct: near
        rts
.endproc
