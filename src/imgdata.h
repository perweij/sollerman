;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.ifndef IMGDATA_LOADED
.define IMGDATA_LOADED

.import _frame0000              ; in "boat.c"

imgcol0= _frame0000
imgcol1= _frame0000+1
imgdat0= _frame0000+2
coldat0= _frame0000+3002

.define imgwidth 120
.define scrollwidth 130

.endif
