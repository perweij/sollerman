;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.include "doublebuffer.h"
.include "effect_rasterbar.h"


;;; Interface ;;;
.export introtext_init
.export introtext_play
.export introtext_destruct

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.segment "DATA"

introtextframecount:
        .byt 0
introtextindex:
        .byt 0
introlinelength= 30
introtext0:
        .asciiz       "songname: sollerman           "
introtext1:
        .asciiz       "by:       hedgewizard/sgc-1000"
introtext2:
        .asciiz       "release:  forndata summer 2021"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.segment "CODE"


.proc introtext_init: near
        jsr doublebuffer_init

        lda #$d5                ; lower case
        sta $ff13

        ;; reset y-scroll
        lda #00
        sta $ff07

        rts
.endproc


.proc introtext_destruct: near
        ;; delete introtext
        ldx #00
        lda #32
@loop:
        sta $4c00+(40 * 2)+5,x
        sta $4c00+(40 * 3)+5,x
        sta $4c00+(40 * 4)+5,x
        inx
        cpx #introlinelength
        bne @loop

        rts
.endproc



.proc introtext_play: near
        jsr rasterbar_play

        lda introtextframecount
        cmp #08
        bne @skip
        lda #00
        sta introtextframecount

        lda introtextindex
        cmp #introlinelength
        bcs @skip

        ldx introtextindex
        lda introtext0,x
        sta $4c00+(40 * 2)+5,x
        lda introtext1,x
        sta $4c00+(40 * 3)+5,x
        lda introtext2,x
        sta $4c00+(40 * 4)+5,x

        lda #00
        sta $4800+(40 * 2)+5,x
        sta $4800+(40 * 3)+5,x
        sta $4800+(40 * 4)+5,x

        inc introtextindex

@skip:
        inc introtextframecount
        rts
.endproc
