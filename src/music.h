;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.include "musicdata.h"

.segment "CODE"

.ifndef music_init

.macro music_init subtune
        lda #subtune
        jsr sid_init
.endmacro


.macro music_play cmp0,cmp1
        lda cmp0
        cmp cmp1
        bne @music_play_multi

        jsr sid_play
        jmp @music_play_cont0
@music_play_multi:
        jsr sid_multiplay
@music_play_cont0:
.endmacro

.endif
