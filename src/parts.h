;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.ifndef part_next_index
.import part_next_index
.import parttable

.define part_offset_init 2
.define part_offset_play 4
.define part_offset_destruct 6


.define NR_PARTS 3

.endif
