;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.include "parts.h"
.include "debug.h"

.export sequencer_init
.export sequencer_play
.exportzp demopart


.segment "ZEROPAGE"

demopart:
        .res 1,0
framecount_irq:
        .res 1,0
framecount_irq_hi:
        .res 1,0



.segment "CODE"

.macro load_x_with_index_to_sub offset
        lda part_next_index
        cld
        clc
        adc #offset
        tax
.endmacro

.macro load_x_with_index_to_current_sub offset
        lda part_next_index
        cld
        sec
        sbc #8
        clc
        adc #offset
        tax
.endmacro

        ;; set y=framecount lo of next part, x=same hi
.macro load_yx_with_lohi_of_next_part_framestart
        ldx part_next_index
        lda parttable,x
        tay
        inx
        lda parttable,x
        tax
.endmacro



.macro step_part
        .local step_part_final
        .local step_part_ready

        lda demopart
        cmp #(NR_PARTS - 1)
        bcs step_part_final

        inc demopart
        lda part_next_index
        cld
        clc
        adc #08
        sta part_next_index
        jmp step_part_ready

step_part_final:
        ;; we reached the final demo part,
        ;; skip further checks
        lda #$4C                 ; jmp op code
        sta skip_when_last
        lda #<step_play
        sta skip_when_last + 1
        lda #>step_play
        sta skip_when_last + 2
step_part_ready:
.endmacro


.macro step_init
        .local init
        .local init_ready

        load_x_with_index_to_sub part_offset_init
        stx init+1

        lda #>init_ready
        pha
        lda #<init_ready-1
        pha
init:
        jmp (parttable)                   ;smc
init_ready:
        ;; setup play smc
        cld
        load_x_with_index_to_sub part_offset_play
        stx play + 1

.endmacro


.proc sequencer_init: near
        step_init
        step_part
        ;; demopart points to current part
        lda #0
        sta demopart
.endproc


sequencer_play:
        dbg_part demopart

skip_when_last:   ; SMC (jmp created here on last demopart)
        load_yx_with_lohi_of_next_part_framestart
        cpy framecount_irq
        bne step_play
        cpx framecount_irq_hi
        bne step_play

        ;; Destruct
        load_x_with_index_to_current_sub part_offset_destruct
        stx @destruct+1

        lda #>@destruct_ready
        pha
        lda #<@destruct_ready-1
        pha
@destruct:
        jmp (parttable)                   ;smc
@destruct_ready:

        step_init
        step_part

step_play:
        lda #>play_ready
        pha
        lda #<play_ready-1
        pha
play:  jmp (parttable)          ; smc
play_ready:

        inc framecount_irq
        bne :+
        inc framecount_irq_hi
:

        dbg_part 255
        rts
