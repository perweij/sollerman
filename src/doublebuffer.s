;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.export doublebuffer_init
.export _doublebuffer
.export clear_buf
.export write_chrmem
.export write_colmem
.export read_chrmem
.export read_colmem


;;; double buffering
.DEFINE BUF2CHRHI  $4c
.DEFINE BUF2COLHI  $48


.segment "DATA"

write_chrmem:
        .byt $0c
write_colmem:
        .byt $08
read_chrmem:
        .byt BUF2CHRHI
read_colmem:
        .byt BUF2COLHI


_bufcount:
        .byt 0


.segment "CODE"


.proc doublebuffer_init: near
        jsr _doublebuffer
        jsr clear_buf

        jsr _doublebuffer
        jsr clear_buf

        lda #00
        sta _bufcount

        rts
.endproc




.proc _doublebuffer: near
        lda _bufcount
        and #01
        beq @b0

        lda #BUF2COLHI
        sta read_colmem
        lda #BUF2CHRHI
        ldx #$0c
        ldy #$08
        jmp @do_set
@b0:
        lda #$08
        sta read_colmem
        lda #$0c
        ldx #BUF2CHRHI
        ldy #BUF2COLHI
@do_set:
        sta read_chrmem
        stx write_chrmem
        sty write_colmem

        sta $ff14               ; point TED to next read video matrix

        inc _bufcount

        rts
.endproc



clear_buf:
        lda write_chrmem
        sta clearloop0+2
        sta clearloop0+5
        cld
        clc
        adc #01
        sta clearloop0+8
        cld
        clc
        adc #01
        sta clearloop0+11

        lda write_colmem
        sta clearloop0+14
        sta clearloop0+17
        cld
        clc
        adc #01
        sta clearloop0+20
        cld
        clc
        adc #01
        sta clearloop0+23

        lda #$20
        ldx #0
clearloop0:
        sta $0c00,x
        sta $0cfa,x
        sta $0df4,x
        sta $0eee,x

        sta $0800,x
        sta $08fa,x
        sta $09f4,x
        sta $0aee,x
        inx
        cpx #250
        bne clearloop0
        rts
