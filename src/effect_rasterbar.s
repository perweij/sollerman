;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.include "rasterconf.h"
.include "cols.h"
.include "irq.h"

.export rasterbar_play


.proc rasterbar_play: near
        lda rasterlineindex
        cmp #1
        bne @nextbar0

        lda_col BLUEb,6
        sta $ff19
        sta $ff15


@nextbar0:
        cmp #LINE_OCEAN
        bne @nextbar1

        ldx $ff1d
        inx
        inx
        stx $b0
:       ldx $ff1d
        cpx $b0
        bne :-

        lda_col BLUEa,0
        sta $ff15
        sta $ff19
        rts


@nextbar1:
        rts
.endproc
