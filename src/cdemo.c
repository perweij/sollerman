// Author:  Per Weijnitz <per.weijnitz@gmail.com>
// Source:  cc65 (https://cc65.github.io)
// Repo:    https://gitlab.com/perweij/sollerman
// License: GNU GENERAL PUBLIC LICENSE Version 3

#include <plus4.h>

#define PLAY_PETSCII_RASTERLINE 250

extern void irqinit();
extern void playpetscii();



void main ( void ) {
  unsigned char ucMsb = ( unsigned char ) ( ( PLAY_PETSCII_RASTERLINE & 0x0100 ) >> 8 );
  unsigned char ucLsb = ( unsigned char ) (   PLAY_PETSCII_RASTERLINE & 0x00ff );

  irqinit();

  // main thread loop
  for ( ;; ) {
    // each time we reach PLAY_PETSCII_RASTERLINE,
    // initiate time consuming data copy operation
    // which can be interrupted without visual disruptions
    while ( TED.rasterline_hi != ucMsb && TED.rasterline_lo != ucLsb) {}
    playpetscii();
  }
}
