;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.ifndef _doublebuffer
.import doublebuffer_init
.import _doublebuffer
.import write_chrmem
.import write_colmem
.import read_chrmem
.import read_colmem

.endif
