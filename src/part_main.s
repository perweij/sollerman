;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.include "music.h"
.include "rasterconf.h"
.include "effect_rasterbar.h"
.include "irq.h"

.export main_play
.export main_init
.export main_destruct


.proc main_init: near
        lda #$00     ; select first tune
        jsr sid_init ; init music
        rts
.endproc


.proc main_play: near
        jsr rasterbar_play
        music_play rasterlineindex,#MUSIC_LINE
        rts
.endproc


.proc main_destruct: near
        rts
.endproc
