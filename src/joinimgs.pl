#!/usr/bin/perl -w
# Author:  Per Weijnitz <per.weijnitz@gmail.com>
# Source:  perl5
# Repo:    https://gitlab.com/perweij/sollerman
# License: GNU GENERAL PUBLIC LICENSE Version 3

use strict;
use warnings;


my @files = @ARGV;

my $dat = {};

my @head = ();
my @tail = ();

foreach my $f (@files) {
    open(F, $f);
    chomp(my @d = <F>);
    close(F);

    @head = splice(@d, 0, 2);
    @tail = splice(@d, -2, 2);

    my $r = join("", @d);
    my @dn = split(",", $r);

    for(my $offset = 0; $offset < 2000; $offset += 1000) {
        for(my $row = 0; $row < 25; $row++) {
            for(my $col = 0; $col < 40; $col++) {
                my $val = 0 + ($dn[$offset + ($row * 40) + $col]);
                push(@{$dat->{$offset}->{$row}}, $val);
            }
        }
    }
}

print "// files: ".join(" ", @files)."\n";
print "// offsetchar: 2   offsetcol: ".((@files * 40 * 25) + 2)."\n";
print "unsigned int frame0000_rowlength = ".(40 * @files).";\n";
print join("\n", @head);
my @result = ();
foreach my $offset (sort {$a <=> $b} keys %{$dat}){
    foreach my $row (sort  {$a <=> $b} keys %{$dat->{$offset}}) {
        push(@result, @{$dat->{$offset}->{$row}});
    }
}
print join(",", @result),"\n";
print join("\n", @tail),"\n";
