;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.ifndef dec16

.macro dec16 addr
        sec
        lda addr
        sbc #<$0001
        sta addr
        lda addr+1
        sbc #>$0001
        sta addr+1
.endmacro


.macro add16 addr, val
        cld
        clc
        lda addr
        adc val
        sta addr
        lda addr+1
        adc #$00
        sta addr+1
.endmacro

.endif
