;;; Author:  Per Weijnitz <per.weijnitz@gmail.com>
;;; Source:  ca65 (https://cc65.github.io/doc/ca65.html)
;;; Repo:    https://gitlab.com/perweij/sollerman
;;; License: GNU GENERAL PUBLIC LICENSE Version 3

.include "part_introtext.h"
.include "part_intro.h"
.include "part_main.h"

.export part_next_index
.export parttable


.segment "DATA"
        ;;  index to the upcoming part
part_next_index:
        .byt 0


        ;; defined in src/plus4/plus4.cfg:
.segment "DATAALIGNED"


.ASSERT (* .MOD $100) = 0, error, "jump table not page aligned"

parttable:
        .byt 0, 0                ; start at frame (16bit)
        .word introtext_init
        .word introtext_play
        .word introtext_destruct

        .byt $00, $02
        .word intro_init
        .word intro_play
        .word intro_destruct

        .byt $50, $0a
        .word main_init
        .word main_play
        .word main_destruct
