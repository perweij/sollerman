# Author:  Per Weijnitz <per.weijnitz@gmail.com>
# Source:  GNU Make
# Repo:    https://gitlab.com/perweij/sollerman
# License: GNU GENERAL PUBLIC LICENSE Version 3

DISK = disk/sgc-1000_forndata21.d64
DISKID = "hedgewizard/sgc,1000"
DISKEXE = sollerman

##################################################################################
.ONESHELL:
$(shell mkdir -p bin)
include Makefile.stdcc65
include Makefile.dockerstuff

# install these here programs and point out accordingly
EXOMIZER = ../../root/bin/exomizer
C1541    = c1541
##################################################################################


all: | src/boat.c $(PROGRAM) $(PROGRAM).exo package


src/boat.c:
	perl src/joinimgs.pl img/nord_left.c img/nord_mid.c img/nord_right.c > $@


$(PROGRAM).exo: $(PROGRAM)
	$(EXOMIZER) sfx systrim $< music/groovedata.tzp -t4 -o $@ -x3


package: $(DISK)


$(DISK): $(PROGRAM)
	mkdir -p disk
	$(C1541) -format "$(DISKID)" d64 $@
	$(C1541) -attach $@ -write $(PROGRAM).exo $(DISKEXE)
	touch empty.txt
	$(C1541) -attach $@ -write empty.txt "=             -="
	$(C1541) -attach $@ -write empty.txt "= a tribute   |="
	$(C1541) -attach $@ -write empty.txt "= to the best %="
	$(C1541) -attach $@ -write empty.txt "= captain on  /="
	$(C1541) -attach $@ -write empty.txt "= the worst   ~="
	$(C1541) -attach $@ -write empty.txt "= boat.       *="
	$(C1541) -attach $@ -write empty.txt "=             .="
	$(C1541) -attach $@ -write empty.txt "= released at ;="
	$(C1541) -attach $@ -write empty.txt "= forndata2021 ="
	$(C1541) -attach $@ -write empty.txt "=             ,="
	$(C1541) -attach $@ -write empty.txt "= hedgewizard +="
	$(C1541) -attach $@ -write empty.txt "= sgc-1000    !="
	$(C1541) -attach $@ -write empty.txt "=             '="
	rm -f empty.txt
